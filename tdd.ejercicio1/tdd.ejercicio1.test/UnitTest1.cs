﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tdd.ejercicio1.test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Ejercicio1 ejercicio = new Ejercicio1("casa");
            ResultadosEjercicio1 resultados = ejercicio.Contar();
            Assert.AreEqual(2, resultados.NumeroConsonantes);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception), "Que dice Mari que Palabra nula o vacia")]
        public void NoNuloOVacio()
        {
            Ejercicio1 ejercicio = new Ejercicio1(null);
            ejercicio.Inicializar();
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception), "Que dice Mari que la pabraba excede el máximo de caracteres.")]
        public void NoMayor50()
        {
            Ejercicio1 ejercicio = new Ejercicio1("odfghjklgdfhkjljghafsgalskasafghjskajshghashjashjaghfgfsahjsagh");
            ejercicio.Inicializar();
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public void SoloLetras()
        {
            Ejercicio1 ejercicio = new Ejercicio1("12131!");
            ejercicio.Inicializar();
        }
    }
}
