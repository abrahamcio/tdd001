﻿using System;

namespace tdd.ejercicio1
{
    class EjecutaEjercicio1
    {
        static void Main(string[] args)
        {
            var sigo = "";
            do
            {
                try
                {
                    Ejercicio1 prueba001 = new Ejercicio1("");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba002 = new Ejercicio1(null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba003 = new Ejercicio1("47668");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba004 = new Ejercicio1("Hola Mundo");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba005 = new Ejercicio1("Hola Mundo 005");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba006 = new Ejercicio1("HlMnd");
                    ImprimeResultados(prueba006.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba007 = new Ejercicio1("iaiaio");
                    ImprimeResultados(prueba007.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba008 = new Ejercicio1("Hola");
                    ImprimeResultados(prueba008.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba009 = new Ejercicio1("Hhhhhhhhholaaaaaaaaaaa");
                    ImprimeResultados(prueba009.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba010 = new Ejercicio1("Canción");
                    ImprimeResultados(prueba010.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    Ejercicio1 prueba011 = new Ejercicio1("Pingüino");
                    ImprimeResultados(prueba011.Contar());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("\r\nPara volver a ejecutar presione C, Para salir Q\r\n");
                sigo = Console.ReadKey().KeyChar.ToString().ToLower();
            } while (sigo == "c");


        }

        private static void ImprimeResultados(ResultadosEjercicio1 resultadosEjercicio1)
        {
            Console.WriteLine($"Vocales [{resultadosEjercicio1.NumeroVocales}] Consonantes [{resultadosEjercicio1.NumeroConsonantes}]");
        }
    }
}
