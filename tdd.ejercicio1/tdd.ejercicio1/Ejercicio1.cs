﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tdd.ejercicio1
{
    public class ResultadosEjercicio1
    {
        public int NumeroVocales { get; set; }
        public int NumeroConsonantes { get; set; }
    }
    public class Ejercicio1
    {
        string palabraUsuario;
        List<string> vocales = new List<string>{ "a", "e", "i", "o", "u", "á", "é", "í", "ú", "ó","ü" };
        public Ejercicio1(string palabra)
        {
            palabraUsuario = palabra;
        }

        public void Inicializar()
        {
            Console.WriteLine($"Palabra recibida [{palabraUsuario}]");
            if (string.IsNullOrEmpty(palabraUsuario))
                throw new Exception($"Que dice Mari que Palabra nula o vacia");
            if (palabraUsuario.Contains(" "))
                throw new Exception("Que dice Mari que Palabra tiene espacios");
            if (!System.Text.RegularExpressions.Regex.IsMatch(palabraUsuario, "[a-zA-z]+", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                throw new Exception($"Que dice Mari que Palabra Invalida");
            if (!(palabraUsuario.Length <= 50))
                throw new Exception($"Que dice Mari que la pabraba excede el máximo de caracteres.");
        }

        public ResultadosEjercicio1 Contar()
        {
            ResultadosEjercicio1 resultados = new ResultadosEjercicio1();
            string palabraFormateada = palabraUsuario.ToLower();
            foreach (char letra in palabraFormateada)
            {
                if (vocales.Contains(letra.ToString()))
                {
                    resultados.NumeroVocales++;
                }
                else
                {
                    resultados.NumeroConsonantes++;
                }
            }

            return resultados;
        }
    }
}
